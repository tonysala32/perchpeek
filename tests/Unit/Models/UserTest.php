<?php

namespace Tests\Unit\Models;

use App\Models\Ticket;
use App\Models\User;
use Tests\TestCase;

class UserTest extends TestCase
{
    public function test_a_user_can_belong_to_a_ticket()
    {
        $user = User::factory()->create();
        $ticket = Ticket::factory()->create(['user_id' => $user->id]);

        $this->assertCount(1, $user->tickets);
    }
}
