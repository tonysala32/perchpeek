<?php

namespace Tests\Unit\Models;

use App\Models\Ticket;
use App\Models\User;
use Tests\TestCase;

class TicketTest extends TestCase
{
    public function test_a_ticket_has_a_user()
    {
        $user = User::factory()->create();
        $ticket = Ticket::factory()->create(['user_id' => $user->id]);

        $this->assertEquals($ticket->user->toArray(), $user->toArray());
    }
}
