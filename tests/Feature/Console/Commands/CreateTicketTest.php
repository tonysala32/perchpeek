<?php

namespace Tests\Feature\Console\Commands;

use App\Models\Ticket;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateTicketTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        User::factory()->create();
    }

    /**
     * Test create ticket command
     *
     * @return void
     */
    public function test_create_ticket_command()
    {
        $this->assertEquals(0, Ticket::count());
        $this->artisan('ticket:create')
            ->assertExitCode(0);
        $this->assertEquals(1, Ticket::count());
    }
}
