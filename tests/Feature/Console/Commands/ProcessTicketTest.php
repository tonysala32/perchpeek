<?php

namespace Tests\Feature\Console\Commands;

use App\Models\Ticket;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProcessTicketTest extends TestCase
{
    /**
     * Test process ticket command
     *
     * @return void
     */
    public function test_process_ticket_command()
    {
        $ticket = Ticket::factory()->create([ 'id' => 1, 'status' => false ]);
        $this->assertEquals(1, Ticket::count());
        $this->artisan('ticket:process')
            ->assertExitCode(0);
        $this->assertEquals(1, Ticket::count());
        $this->assertEquals(true, $ticket->fresh()->status);
    }
}
