<?php

namespace Tests\Feature\Controllers;

use App\Models\Ticket;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TicketsTest extends TestCase
{

    public function test_can_get_all_open_tickets()
    {
        $ticket = Ticket::factory()->create(['status' => false]);
        Ticket::factory()->create(['status' => true]);

        $response = $this->json('GET', '/api/tickets/open');

        $response->assertOk();
        $response->assertJsonCount(1, 'data');
        $response->assertJsonFragment(['status' => false]);
        $response->assertJsonFragment(['subject' => $ticket->subject]);
    }

    public function test_can_get_all_closed_tickets()
    {
        $ticket = Ticket::factory()->create(['status' => true]);
        Ticket::factory()->create(['status' => false]);

        $response = $this->json('GET', '/api/tickets/closed');

        $response->assertOk();
        $response->assertJsonCount(1, 'data');
        $response->assertJsonFragment(['status' => true]);
        $response->assertJsonFragment(['subject' => $ticket->subject]);
    }
}
