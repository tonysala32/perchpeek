<?php

namespace Tests\Feature\Controllers;

use App\Models\Ticket;
use App\Models\User;
use Tests\TestCase;

class UsersTest extends TestCase
{
    public function test_can_get_all_tickets_belonging_to_a_user()
    {
        $userOne = User::factory()->create();
        $userTwo = User::factory()->create();
        Ticket::factory()->create(['status' => true, 'user_id' => $userOne->id]);
        Ticket::factory()->create(['status' => false, 'user_id' => $userTwo->id]);

        $response = $this->json('GET', '/api/users/' . $userOne->email . '/tickets');

        $response->assertOk();
        $response->assertJsonCount(1, 'data');
        $response->assertJsonFragment(['user_name' => $userOne->name]);
    }
}
