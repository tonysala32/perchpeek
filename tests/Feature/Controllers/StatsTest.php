<?php

namespace Tests\Feature\Controllers;

use App\Models\Ticket;
use App\Models\User;
use Carbon\Carbon;
use Tests\TestCase;

class StatsTest extends TestCase
{
    public function test_can_get_stats()
    {
        $userOne = User::factory()->create();
        $userTwo = User::factory()->create();
        $userThree = User::factory()->create();

        $now = Carbon::now();
        $later = $now->addMinute();

        $ticketSetOne = Ticket::factory(3)->create([
            'status' => true,
            'user_id' => $userOne->id,
            'updated_at' => $now,
        ]);
        $ticketSetTwo = Ticket::factory(5)->create([
            'status' => false,
            'user_id' => $userTwo->id,
            'updated_at' => $now,
        ]);
        $ticketSetThree = Ticket::factory(7)->create([
            'status' => false,
            'user_id' => $userThree->id,
            'updated_at' => $now,
        ]);
        $latestProcessedTicket = Ticket::factory()->create([
            'status' => true,
            'updated_at' => $later,
        ]);

        $response = $this->json('GET', '/api/stats');

        $response->assertOk();
        $response->assertJsonCount(4, 'data');
        $response->assertJsonFragment([
            'ticket_count' => Ticket::count(),
        ]);
        $response->assertJsonFragment([
            'unprocessed_ticket_count' => $ticketSetTwo->count() + $ticketSetThree->count(),
        ]);
        $response->assertJsonFragment([
            'user_with_most_submissions' => $userThree->name,
        ]);
        $response->assertJsonFragment([
            'last_ticket_processed_at' => $latestProcessedTicket->updated_at,
        ]);
    }
}
