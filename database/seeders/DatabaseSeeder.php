<?php

namespace Database\Seeders;

use App\Models\Ticket;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Create 5 users
        User::factory(5)->create();

        // Create 25 tickets
        for ($i = 0; $i < 25; $i++) {
            Ticket::factory()->create(['user_id' => User::inRandomOrder()->first()->id]);
        }
    }
}
