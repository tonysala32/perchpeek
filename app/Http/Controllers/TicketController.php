<?php

namespace App\Http\Controllers;

use App\Http\Resources\TicketResource;
use App\Services\TicketService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \App\Services\TicketService $ticketService
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getOpen(TicketService $ticketService): AnonymousResourceCollection
    {
        return TicketResource::collection($ticketService->getAllOpen()->paginate());
    }

    /**
     * Display a listing of the resource.
     *
     * @param \App\Services\TicketService $ticketService
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getClosed(TicketService $ticketService): AnonymousResourceCollection
    {
        return TicketResource::collection($ticketService->getAllClosed()->paginate());
    }
}
