<?php

namespace App\Http\Controllers;

use App\Http\Resources\TicketResource;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class UserController extends Controller
{
    /**
     * Return tickets belonging to a user
     *
     * @param \App\Models\User          $user
     * @param \App\Services\UserService $userService
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getUsersTickets(User $user, UserService $userService): AnonymousResourceCollection
    {
        return TicketResource::collection($userService->getUserTickets($user)->paginate());
    }
}
