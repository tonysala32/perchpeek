<?php

namespace App\Http\Controllers;

use App\Services\TicketService;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;

class StatsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \App\Services\UserService   $userService
     * @param \App\Services\TicketService $ticketService
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(UserService $userService, TicketService $ticketService): JsonResponse
    {
        return response()->json([
            'data' => [
                'ticket_count' => $ticketService->count(),
                'unprocessed_ticket_count' => $ticketService->getAllOpen()->count(),
                'user_with_most_submissions' => $userService->getUserWithMostTicketSubmissions()->name,
                'last_ticket_processed_at' => $ticketService->getLastProcessedTicket()->updated_at,
            ],
        ]);
    }
}
