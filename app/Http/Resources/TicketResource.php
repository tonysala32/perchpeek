<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TicketResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'subject' => $this->subject,
            'content' => $this->content,
            'user_name' => $this->user->name,
            'user_email' => $this->user->email,
            'created' => $this->created_at,
            'status' => $this->status,
        ];
    }
}
