<?php

namespace App\Console\Commands;

use App\Models\Ticket;
use Illuminate\Console\Command;

class ProcessTicket extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ticket:process';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process a ticket.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $query = Ticket::query();
        $query->where('status', false);
        $query->orderBy('created_at');
        $ticket = $query->first();

        $ticket?->setAttribute('status', true)->save();

        return 0;
    }
}
