<?php

namespace App\Console\Commands;

use App\Models\Ticket;
use App\Models\User;
use Illuminate\Console\Command;

class CreateTicket extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ticket:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a ticket.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        Ticket::factory()->create([
            'user_id' => User::inRandomOrder()->first()->id,
        ]);

        return 0;
    }
}
