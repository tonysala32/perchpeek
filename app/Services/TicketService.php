<?php

namespace App\Services;

use App\Models\Ticket;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

class TicketService implements ServiceInterface
{
    public function __construct(protected Ticket $ticket)
    {}

    /**
     * @return mixed
     */
    public function getAllOpen(): Builder {
        return $this->ticket->where('status', false);
    }

    /**
     * @return mixed
     */
    public function getAllClosed(): Builder {
        return $this->ticket->where('status', true);
    }

    /**
     * @return \App\Models\Ticket|null
     */
    public function getLastProcessedTicket(): ?Ticket
    {
        return $this->ticket->where('status', true)->orderBy('updated_at', 'desc')->first();
    }

    /**
     * @return int
     */
    public function count(): int
    {
        return $this->ticket->count();
    }
}