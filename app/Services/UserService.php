<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Database\Eloquent\Relations\HasMany;

class UserService implements ServiceInterface
{
    public function __construct(protected User $model)
    {}

    /**
     * Return the tickets belonging to the $user
     *
     * @param \App\Models\User $user
     *
     * @return
     */
    public function getUserTickets(User $user): HasMany
    {
        return $user->tickets();
    }

    /**
     * @return \App\Models\User|null
     */
    public function getUserWithMostTicketSubmissions(): ?User
    {
        return $this->model->withCount('tickets')->orderBy('tickets_count', 'desc')->first();
    }
}