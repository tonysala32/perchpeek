### General Rules
- Implement the solution using Laravel 8 Framework;
- Write unit tests using PHPUnit Framework;
- Ensure your testing coverage is above 80%;
- Including Unit and Integration tests.
- Generate dummy data using model factories.

### Submission

Please email a zipped version (git bundle) of the code base including the .git folder.
Please include a README file to provide any instruction to the code reviewer.

### Challenge

 - Create a console command that generates a ticket with dummy data every minute. A ticket should have the following fields:
    - Ticket Subject
    - Ticket Content
    - Name of the user who submitted the ticket
    - Email of the user who submitted the ticket
    - Time when the ticket was added
    - Status of the ticket (boolean) - Set to false by default to indicate that the ticket is
not processed.


 - Create another console command that processes a ticket 5 every five minutes. Tickets should be processed in chronological order. Changing the status value to true would be considered as processing of the ticket.
 - ### Create the following api endpoints:
 - `/tickets/open` - Returns a paginated list of all unprocessed tickets (i.e. all tickets with status set to false)
 - `/tickets/closed` - Returns a paginated list of all processed tickets (i.e. all tickets with status set to true)

 - `/users/{email}/tickets` - Returns a paginated list of all tickets that belong to the user with the corresponding email address.
 - `/stats` - Returns the following stats:
    - total number of tickets in the database;
    - total number of unprocessed tickets in the database;
    - name of the user who submitted the highest number of tickets (count by
email); and
    - time when the last processing of a ticket was done.

## Instruction for running

 - Add file `.env` at the root of the project, containing:
   ```ini

   APP_NAME="Perchpeek Test"
   APP_ENV=local
   APP_KEY=base64:7XgCzbE3CuoEil3lh3Cohx1fsNXHndePYE32HnfuTKI=
   APP_DEBUG=true
   APP_URL=http://localhost

   LOG_CHANNEL=stack
   LOG_LEVEL=debug

   DB_CONNECTION=mysql
   DB_HOST=db
   DB_DATABASE=laravel
   DB_USERNAME=root
   DB_PASSWORD=

   OCTANE_SERVER=swoole
   ```

Run on the command line:

 - `docker-compose up --build -d`
 - `docker exec -ti perchpeek-php sh -c "php artisan migrate:fresh --seed"`
 - Visit via browser http://0.0.0.0:9000/api/