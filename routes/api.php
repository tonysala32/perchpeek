<?php

use App\Http\Controllers\StatsController;
use App\Http\Controllers\TicketController;
use App\Http\Controllers\UserController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('tickets')->name('tickets.')->group(static function () {
    Route::get('open', [TicketController::class, 'getOpen'])->name('open');
    Route::get('closed', [TicketController::class, 'getClosed'])->name('closed');
});

Route::prefix('users')->name('user.')->group(static function () {
    Route::get('{user}/tickets', [UserController::class, 'getUsersTickets'])->name('tickets');
});

Route::get('stats', [StatsController::class, 'index'])->name('stats');
